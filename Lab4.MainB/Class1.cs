﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;
using Lab4.Component1;
using Lab4.Component2B;


namespace Lab4.MainB
{
    public class Class1
    {
        static void MainB(string[] args)
        {
            var kontener = new Container();
            kontener.RegisterComponent(new Dzwig());
            kontener.RegisterComponent(new ZailWindeB());
        }
    }
}
