﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2B
{
    public partial class ZailWindeB:AbstractComponent, IZasil
    {
        private string in1;
        private string out1;

        public ZailWindeB()
        {
            RegisterProvidedInterface(typeof(IZasil), this);
        }
        public ZailWindeB(string in2, string out2)
        {
            in1 = in2;
            out1 = out2;
        }

        string IZasil.Odlacz()
        {
            return in1;
        }
        string IZasil.Podlacz()
        {
            return out1;
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new ArgumentException("");
        }
    
    }
}
