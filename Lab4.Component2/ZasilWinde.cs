﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public partial class ZasilWinde:AbstractComponent, IZasil
    {
        string in1;
        string out1;

        public ZasilWinde()
        {
            RegisterProvidedInterface(typeof(IZasil), this);
        }
        ZasilWinde(string in2, string out2)
        {
            in1 = in2;
            out1 = out2;
        }

        //explicite
        string IZasil.Odlacz()
        {
            return in1;
        }
        string IZasil.Podlacz()
        {
            return out1;
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

    }
}
