﻿using System;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Component2B;
using Lab4.Contract;
using ComponentFramework;


namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Component1.Dzwig);
        public static Type Component2 = typeof(Component2.ZasilWinde);

        public static Type RequiredInterface = typeof(IZasil);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => (IZasil)component2;
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { ((IContainer)container).RegisterComponents((IComponent)component); };

        public static AreDependenciesResolved ResolvedDependencied = (container) => (container as IContainer).DependenciesResolved;

        #endregion

        #region P3

        public static Type Component2B = typeof(Component2B.ZailWindeB);

        #endregion
    }
}
