﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Dzwig:AbstractComponent,IObsluga
    {
        public IZasil zasilanie;
        public Dzwig()
        {
            this.RegisterRequiredInterface(typeof(IZasil));
        }

        public void Podlacz()
        {
            Console.WriteLine("podlaczono");
        }
        public void Odlacz()
        {
            Console.WriteLine("odlaczono");
        }
        public override void InjectInterface(Type type, object impl)
        {
            if (type == typeof(IZasil))
            {
                zasilanie = (IZasil)impl;
            }
        }


        public int Otworz()
        {
            throw new NotImplementedException();
        }

        public int Zamknij()
        {
            throw new NotImplementedException();
        }
    }
}
